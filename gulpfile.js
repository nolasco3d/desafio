const {
    src,
    dest,
    task,
    series,
    parallel,
    watch
} = require('gulp');

let     sass    = require('gulp-sass'),
        csso    = require('gulp-csso')
;

function compile_styles() {
    return src([
        'scss/**/*.scss'
    ])
    .pipe(sass())
    .pipe(csso())
    .pipe(dest('./css/'));
}

function watch_files() {
    watch('scss/**/*', compile_styles);
}

task('default', series(compile_styles, watch_files));
