# Desafio Front-end

Para testar o projeto, abra o XAMPP e inicie os serviços:
    
    Apache.
    MySQL (host = 127.0.0.1).
    

* Importante configurar o endereço do servidor.

# Importando a Base de Dados

Crie um banco de dados chamado "desafio".

Dentro do diretório *db/* na raiz do projeto, existe uma tabela exportada, basta importá-la no banco.

users.sql

Usuário pré-cadastrado:

Email: admin@admin.com

Senha: admin@123


# Clonando o projeto

Clone o projeto dentro do diretório *htdocs* do XAMPP.

Agora é so testar no navegador: http://localhost/desafio/



