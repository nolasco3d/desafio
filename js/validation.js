console.warn('Validation Loaded');

let email, password, c_password;

function verifyLogin() {
    email = formLog.email.value;
    password = formLog.password.value;

    if(email == "" || email.indexOf('@') == -1 ) {
        formLog.email.focus();
        alert("Por favor digite um email válido.");
        return false;
    }
    if(password == "" || password.lenght <= 5) {
        formLog.password.focus();
        alert("Por favor digite uma senha válida.");
        return false;
    }
    
    return true;

}

function verifyReg() {
    email = formReg.email.value;
    password = formReg.password.value;
    c_password = formReg.c_password.value;

    if (email == "" || email == null || email.indexOf('@') == -1) {
        formReg.email.focus();
        alert("Por favor digite um email válido.");
        return false;
    }
    if (password == "" || password == null || password.length <= 5) {
        formReg.password.focus();
        alert("Por favor digite uma senha válida.");
        return false;
    }
    if (c_password == "" || c_password == null || c_password != password) {
        formReg.c_password.focus();
        alert("Por favor digite uma senha igual a anterior.");
        return false;
    }

    return true;

}