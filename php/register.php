<?php
    // Conecta ao banco
    require "connection.php"; 

    // Verifica se o campo email existe e se esta preenchido
    if (isset($_POST['email']) &&  !empty($_POST['email'])) { 
        // $nome = addslashes($_POST['name']);
        $email = addslashes($_POST['email']);
        $password = md5(addslashes($_POST['password']));

        // Query de consulta ao banco 
        $sql = $pdo->prepare("SELECT * FROM users WHERE user = :email"); 
        $sql->bindValue(":email", $email);
        $sql->execute();

       if(!$sql->rowCount() > 0) {
            // Query de inserção ao banco 
            $sql = $pdo->prepare("INSERT INTO users (email, password) VALUES (:email, :password)");
            $sql->bindValue(":email", $email);
            $sql->bindValue(":password", $password);
            $sql->execute();
            
            header("Location: ../congrats.html");
       } else {
            header("Location: ../error.html");
            exit;
       }
    }
?>