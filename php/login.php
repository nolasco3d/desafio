<?php
    session_start();
    // Conecta ao banco
    require "connection.php";

    // Verifica se o campo email existe e se esta preenchido
    if (isset($_POST['email']) &&  !empty($_POST['email'])) { 
        // Pega os dados senha e email do Formulário
        $email = addslashes($_POST['email']); 
        $password = md5(addslashes($_POST['password']));

        // Query de consulta ao banco 
        $sql = $pdo->prepare("SELECT * FROM users WHERE user = :email "); 
        // AND password = :password
        $sql->bindValue(":email", $email);
        // $sql->bindValue(":password", $password);
        $sql->execute();
        
        // Retorna quantos registros a query tem
        if ($sql->rowCount() > 0) {
            // Retorna o primeiro resultado da requisição e monta o array em $data
            $data = $sql->fetch();

            
            if($data['password'] == $password) {
                
                $_SESSION['dash'] = $data['id'];
            
                header('Location: ../dash.html');
            } else {
                // Retorna erro de senha inválida
                header('Location: ../error.html');
            }
            

        } 
    } else {
        header("Location: ../error.html");
        exit;
    }
?>