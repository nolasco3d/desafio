<?php 
    // Nome do banco e Host
    $dsn = "mysql:dbname=desafio;host=127.0.0.1";
    // Usuário
    $dbuser = "root";
    // Senha
    $dbpass = "";

    try {
        // Conexão
        $pdo = new PDO($dsn, $dbuser, $dbpass);
    } catch (PDOExeption $e) {
        // Tratamento de erro
        echo "ERROR: ".$e->getMessage(); 
        exit;
    }
?>